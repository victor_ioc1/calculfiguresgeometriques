/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ioc.xtec.cat.calculfiguresgeometriques;

import java.util.Scanner;

/**
 *
 * @author T440S
 */
public class Rectangle implements FiguraGeometrica{
    private final double base;
    private final double altura;
    
    public Rectangle() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Introduïu la longitud de la base del rectangle: ");
        this.base = scanner.nextDouble();
        System.out.println("Introduïu la longitud de l'altura del rectangle: ");
        this.altura = scanner.nextDouble();
    }
    
    @Override
    public double calcularArea() {
        return base * altura;
    }
    
    @Override
    public double calcularPerimetre() {
        return (base*2)+(altura*2);
    }
}
