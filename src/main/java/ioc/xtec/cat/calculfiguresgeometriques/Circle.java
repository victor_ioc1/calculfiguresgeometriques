/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ioc.xtec.cat.calculfiguresgeometriques;

import java.util.Scanner;

/**
 *
 * @author T440S
 */
public class Circle implements FiguraGeometrica {
    private final double radi;
    private final double pi = Math.PI;
    
    public Circle() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Introduïu el radi del cercle: ");
        this.radi = scanner.nextDouble();
    }
    
    @Override
    public double calcularArea() {
        return (radi * radi)*pi;
    }
    
    @Override
    public double calcularPerimetre() {
        return (2 * radi)*pi;
    }
}
