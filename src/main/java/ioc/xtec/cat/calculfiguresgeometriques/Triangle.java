/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ioc.xtec.cat.calculfiguresgeometriques;

import java.util.Scanner;

/**
 *
 * @author T440S
 */

public class Triangle implements FiguraGeometrica{
    private final double base;
    private final double altura;
    
    public Triangle() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Introduïu la longitud de la base del triangle: ");
        this.base = scanner.nextDouble();
        System.out.println("Introduïu la longitud de l'altura del triangle: ");
        this.altura = scanner.nextDouble();
    }
    
    @Override
    public double calcularArea() {
        return (base * altura)/2;
    }
    
    @Override
    public double calcularPerimetre() {
        return 3 * base;
    }
}
